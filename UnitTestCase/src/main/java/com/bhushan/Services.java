package com.bhushan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Services {

	@Autowired
	private Repo repo;

	public Employee saveData(Employee entity) {
		Employee save = repo.save(entity);
		return save;
	}
}
