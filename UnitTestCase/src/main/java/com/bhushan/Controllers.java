package com.bhushan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controllers {

	@Autowired
	private Services services;

	@PostMapping("/save")
	public Employee save(@RequestBody Employee entity) {
		return services.saveData(entity);
	}

}
