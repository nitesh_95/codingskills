package com.bhushan;

import org.springframework.data.jpa.repository.JpaRepository;

public interface Repo extends JpaRepository<Employee, Long> {

}
