package com.bhushan;

import org.springframework.data.repository.CrudRepository;

public interface Repo extends CrudRepository<User, Long>{

}
