package com.bhushan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndexLocationApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndexLocationApplication.class, args);
	}

}
