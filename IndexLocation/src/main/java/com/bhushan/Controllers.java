package com.bhushan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Controllers {

	@Autowired
	private Repo repo;
	
	@RequestMapping("/")
	public String welcomePage() {
		return "addUser";
	}

	@RequestMapping("/signup")
	public String form(User user) {
		return "add-user";
	}

	@PostMapping("/addDetails")
	public String adduser(User user, Model model, BindingResult result) {
		if (result.hasErrors()) {
			return "add-user";
		} else {
			repo.save(user);
			return "index";
		}
	}

		@PostMapping("/signupPage")
		public String getData() {
			return "add-user";
	}
}
