package com.bhushan;

class Test {

	public int addition(int x, int y) {
		return x + y;
	}

}

class Addition {
	public static void main(String[] args) {
		Test test = new Test();
		int addition = test.addition(20, 30);
		System.out.println(addition);

	}
}