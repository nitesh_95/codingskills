package com.bhushan;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class NewController {

	@Autowired
	private Services services;

	@PostMapping("/save")
	public String saveHomeData(@ModelAttribute("Employee") Employee employee) {
		services.getData(employee);
		return "redirect:/";
	}

	@RequestMapping("/new")
	public String showNewProductPage(Model model) {
		Employee employee = new Employee();
		model.addAttribute("employee", employee);

		return "new_product";
	}

	@GetMapping("/")
	public String viewHomePage(Model model) {
		List<Employee> employees = services.findall();
		model.addAttribute("employees", employees);
		return "index";
	}

	@PostMapping("/delete/{id}")
	public String deleteById(@PathVariable(name = "id") Long id) throws RecordNotFoundException {
		services.deletebyid(id);
		return "redirect:/";
	}

}
