package com.bhushan;

import org.springframework.data.jpa.repository.JpaRepository;

public interface Repos extends JpaRepository<Employee, Long> {

}
