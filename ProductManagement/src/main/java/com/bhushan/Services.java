package com.bhushan;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Services {

	@Autowired
	private Repos repos;

	public Employee getData(Employee entity) {
		Employee employee = repos.save(entity);
		return employee;

	}

	public List<Employee> findall() {
		return repos.findAll();
	}

	public Employee findbyId(Long id) throws RecordNotFoundException {

		Optional<Employee> employee = repos.findById(id);
		{
			if (employee.isPresent()) {
				return employee.get();
			} else {
				throw new RecordNotFoundException("Record not Found");
			}
		}

	}

	public Employee findById(Long id) {
		return repos.findById(id).get();

	}

	public boolean deletebyid(Long id) throws RecordNotFoundException {
		Optional<Employee> employee = repos.findById(id);
		if (employee.isPresent()) {
			repos.deleteById(id);
			return true;
		} else {
			throw new RecordNotFoundException("Record not Found");
		}
		
	}

	public Employee update(Employee enEmployee) throws RecordNotFoundException {
		Optional<Employee> employee = repos.findById(enEmployee.getId());
		if (employee.isPresent()) {
			repos.save(enEmployee);
		} else {
			throw new RecordNotFoundException("Record Not Found");
		}
		return enEmployee;
	}
}
