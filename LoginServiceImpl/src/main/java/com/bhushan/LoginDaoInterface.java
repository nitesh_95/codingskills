package com.bhushan;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface LoginDaoInterface extends JpaRepository<Login, Long> {

	@Query(value = "select * from login_data where id=1", nativeQuery = true)
	public Login getCredentials(String name);

	@Transactional
	@Modifying
	@Query(value = "UPDATE login_data SET password =:password WHERE id =1", nativeQuery = true)
	public int updateCredentials(@Param(value = "password") String password);

	@Query(value = "select * from login_data where id=1", nativeQuery = true)
	public LoginResponse fetchLoginDetails(String name, String password);

}
