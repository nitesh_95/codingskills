package com.bhushan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

	@Autowired
	private LoginDaoInterface loginInterface;

	public Login getCredentials(String name) {
		Login credentials = loginInterface.getCredentials(name);
		return credentials;
	}

	
	public boolean updateNewPassword(Long id, String password) {
		int count = 0;

		try {
			System.out.println(id);
			System.out.println(password);
			TripleDES tripleDES = new TripleDES();
			String encrypt = tripleDES.encrypt(password);
			System.out.println(encrypt);
			System.out.println(id);
			loginInterface.updateCredentials(encrypt);
			System.out.println("updated");

		} catch (Exception e) {
			System.out.println("Data not Saved");
		}
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public LoginResponse fetchLoginCredentials(String name, String password) throws Exception {
		Login ss = loginInterface.getCredentials(name);
		System.out.println("password is " + ss.getPassword());
		System.out.println("name is" + ss.getName());
		LoginResponse loginResponse = new LoginResponse();
		TripleDES des = new TripleDES();
		String encrypt = des.encrypt(password);
		System.out.println(encrypt);
		if (ss.getName().equals(name) && ss.getPassword().equals(encrypt)) {
			loginResponse.setStatus("00");
			loginResponse.setReason("Success");
		} else {
			loginResponse.setStatus("01");
			loginResponse.setReason("Failure");
		}
		return loginResponse;

	}
}