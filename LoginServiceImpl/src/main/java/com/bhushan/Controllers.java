package com.bhushan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controllers {

	@Autowired
	private LoginService loginService;

	@PostMapping("/getCredentials/{name}")
	public Login getCredentials(@PathVariable(name = "name") String name) {
		Login credentials = loginService.getCredentials(name);
		return credentials;
	}


	@PutMapping("/updateData")
	public LoginResponse updatePassword(@RequestBody Login login) {
		LoginResponse loginResponse = new LoginResponse();

		boolean isUpdated = loginService.updateNewPassword(login.getId(), login.getPassword());
		if (isUpdated) {
			loginResponse.setStatus("00");
			loginResponse.setReason("Success");

		}
		return loginResponse;
	}

	@PostMapping("/fetchLoginCredentials/{name}/{password}")
	public LoginResponse fetchLoginCredentials(@PathVariable(name = "name") String name,
			@PathVariable(name = "password") String password) throws Exception {
		LoginResponse fetchLoginCredentials = loginService.fetchLoginCredentials(name, password);
		return fetchLoginCredentials;

	}
}
