package com.bhushan;

import org.springframework.beans.factory.annotation.Autowired;

public class LoginDao {

	@Autowired
	private LoginDaoInterface loginDaoInterface;

	public Login getCredentials(String name) {
		Login credentials = loginDaoInterface.getCredentials(name);
		return credentials;
	}
}
