package com.bhushan;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class RecordnotfoundException extends Exception {

	private static final long serialVersionUID = 1L;

	RecordnotfoundException(String message) {
		super(message);
	}
}
