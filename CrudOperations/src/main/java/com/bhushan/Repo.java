package com.bhushan;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface Repo extends JpaRepository<Student, Long> {

	public List<Student> findByName(String name);

	public List<Student> findByAddress(String address);

	public List<Student> findByIdAndName(Long id,String name);
	
	public List<Student> findByNameAndAddress(String name, String address);
}
