package com.bhushan;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Services {

	@Autowired
	private Repo repo;

	public List<Student> getAll() {
		return repo.findAll();
	}

	public List<Student> findByNameAndId(String name, String address) {
		List<Student> findByNameAndAddress = repo.findByNameAndAddress(name, address);
		return findByNameAndAddress;
	}

	public List<Student> findByIdAndName(Long id, String name) {
		List<Student> findByIdAndName = repo.findByIdAndName(id, name);
		return findByIdAndName;
	}

	public Student save(Student student) {
		repo.save(student);
		return student;
	}

	public Student findByid(Long id) throws RecordnotfoundException {
		Optional<Student> student = repo.findById(id);
		if (student.isPresent()) {
			return student.get();
		} else {
			throw new RecordnotfoundException("Record not Found");
		}

	}

	public Student update(Student student) throws RecordnotfoundException {
		Optional<Student> stOptional = repo.findById(student.getId());
		if (stOptional.isPresent()) {
			repo.save(student);
			return student;
		} else {
			throw new RecordnotfoundException("Record Not Found");
		}

	}

	public void delete(Long id) throws RecordnotfoundException {
		Optional<Student> student = repo.findById(id);
		if (student.isPresent()) {
			repo.deleteById(id);
		} else {
			throw new RecordnotfoundException("Record not Found");
		}
	}

	public List<Student> byName(String name) {
		List<Student> students = repo.findByName(name);
		return students;

	}

	public List<Student> byAddress(String address) {
		List<Student> student = repo.findByAddress(address);
		return student;
	}

	public Long countByRow() {
		long count = repo.count();
		return count;
	}
}
