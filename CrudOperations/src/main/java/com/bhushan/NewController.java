package com.bhushan;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NewController {

	@Autowired
	private Services services;

	@PostMapping("/getData/{id}")
	@ResponseBody
	public List<Student> findbynameid(@PathVariable(name  = "id") Long id,
			@RequestParam(defaultValue = "name") String name) {
		List<Student> students = services.findByIdAndName(id, name);
		return students;
	}

	@PostMapping("/getByNameAndAddress")
	public List<Student> getbyNameandAddress(@RequestParam(defaultValue = "name") String name,@RequestParam(defaultValue = "address") String address){
		List<Student> students = services.findByNameAndId(name, address);
		return students;
	}
	@GetMapping("/findAll")
	public List<Student> getAll() {
		return services.getAll();
	}

	@PostMapping("/saveData")
	public Student saveData(@RequestBody Student student) {
		services.save(student);
		return student;
	}

	@PostMapping("updateData")
	public Student updateData(@RequestBody Student student) throws RecordnotfoundException {
		services.update(student);
		return student;
	}

	@DeleteMapping("deleted/{id}")
	public HttpStatus deletebyid(@PathVariable(name = "id") Long id) throws RecordnotfoundException {
		services.delete(id);
		return HttpStatus.MOVED_PERMANENTLY;
	}

	@PostMapping("/findByName/{name}")
	public List<Student> findByName(@PathVariable(name = "name") String name) {
		List<Student> students = services.byName(name);
		return students;

	}

	@PostMapping("findByAddress/{address}")
	public List<Student> findByaddress(@PathVariable(name = "address") String address) {
		List<Student> students = services.byAddress(address);
		return students;
	}

	@GetMapping("/getCount")
	public Long count() {
		return services.countByRow();
	}

	@PostMapping("/findByIdAndName/{id}/{name}")
	public List<Student> findByIdAndName(@PathVariable(name = "id") Long id, @PathVariable(name = "name") String name) {
		List<Student> findByIdAndName = services.findByIdAndName(id, name);
		return findByIdAndName;
	}

	@PostMapping("/findByNameAndId/{name}/{address}")
	public List<Student> findByNameAndId(@PathVariable(name = "name") String name,
			@PathVariable(name = "address") String address) {
		List<Student> findByNameAndAddress = services.findByNameAndId(name, address);
		return findByNameAndAddress;
	}
}